<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2019-12-05
 * Time: 21:58
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class BenchmarkRestController extends AbstractController
{


    /**
     * @var RequestStack
     */
    private $request;

    public function __construct(RequestStack $request)
    {

        $this->request = $request;
    }

    /**
     * @Route(name="benchmark",path="/benchmark")
     */
    public function index(){

        $json = "{\"dane_hex\":\"670D000A00000000F503B43A00000000000000000000102700003837E45D00000000001FB4CE0000966500\",\"czas\":\"2019-12-01 22:57:14\",\"wersja_ramki\":5,\"zrodlo\":\"serwer\/info\/d\/5\/10\",\"id_terminal\":333,\"id_firma\":0,\"dane\":{\"osg\":true,\"predkosc\":0,\"kod_op_gsm\":26006,\"czas\":\"2019-12-01 22:57:12\",\"zagps\":false,\"id_firma\":0,\"droga\":0.0,\"ber\":true,\"lac\":1013,\"br\":false,\"oagps\":false,\"liczba_sat_uzy\":4,\"jam\":false,\"rssi_wart\":31,\"azymut\":0,\"wsk_bledu\":0,\"bzp\":false,\"a_v\":false,\"id_terminal\":333,\"nr_rekordu\":3431,\"sr\":false,\"ret\":false,\"rssi\":true,\"aagsm\":false,\"ao\":false,\"opp\":false,\"liczba_sat_wid\":11,\"stan_akum\":0,\"_2d_3d\":false,\"szerokosc\":0,\"odp\":false,\"wysokosc\":0.0,\"awp\":false,\"ldowz\":0,\"oc_ryzyka\":0,\"zw\":3,\"dlugosc\":0,\"przechyl_y\":0,\"cid\":15028,\"przechyl_x\":0},\"nr_rekordu\":3431,\"status\":0}";

//        dump($this->request);

        return new JsonResponse($json);
    }

}